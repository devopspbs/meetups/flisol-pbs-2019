# Lista de Apps para o Installfest do FLISoL Parauapebas

## Apps para Desktop (GNU/Linux, Windows e Mac OS)

### Internet

* Mozilla Firefox
* Chromium

### Escritório

* LibreOffice
  - Writer
  - Calc
  - Impress
  - Draw
  - Base
  - Math
* Scribus
* TeXStudio
* Calibre

### Games

* Secret Maryo Chronicles
* SuperTuxKart (STK)
* Speed Dreams
* VDrift
* AssaultCube
* Alien Arena
* 0 A.D.
* Battle for Wesnoth
* Unknown Horizons
* Minetest
* FlightGear
* OpenTTD
* FreeCiv
* The Dark Mod

### Imagem e Desenho

* GIMP
* Krita
* Inkscape
* digiKam

### Som e Vídeo

* VLC
* Audacity
* OBS Studio
* Kdenlive

### CAD/CAM e 3D

* Blender
* LibreCAD
* OpenSCAD
* FreeCAD
* SweetHome 3D

### Privacidade

* Tor Browser


## Apps para Mobile (Android)

### Lojinha de Apps

* F-Droid

### Mapa e GPS

* OsmAnd+
  - OsmAnd Countour lines
* OSMTracker
* Maps
* Open Map

### Internet

* Mozilla Firefox

### Tela e Câmera

* Open Camera
* Night Screen
* ScreenCam
* ScreenStream
* SecondScreen
* Barcode Scanner 
* SecScanQR
* Obsqr
* Open Note Scanner

### Escritório

* K-9 Mail
* LibreOffice Viewer
* Impress Remote
* Markor
* Etar
* PDF Converter
* Pdf Viewer Plus
* MuPDF viewer
* FBReader

### Games

* SuperTuxKart (STK)
* Battle for Wesnoth
* Minetest
* freeminer
* Gloomy Dungeons 2
* Cow's Revenge

### Chat

* Telegram
* Riot.im
* Rocket.Chat 
* Jitsi Meet*
* Plumble
* Conversations

### Som e Vídeo

* YouTube Cacher
* NewPipe
* AntennaPod
* Video Transcoder
* VLC

### Nuvem

* Nextcloud
  - Nextcloud Talk
  - Nextcloud Notes
  - Nextcloud Bookmarks
  - PhoneTrack
  - News
  - OCReader
  - Nextcloud SMS
* NC Bookmark Viewer

### Privacidade

* Orbot
* Tor Browser

_*no momento não está disponível no F-Droid_
