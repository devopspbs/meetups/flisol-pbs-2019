#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Arquivo dos slid's 
Tipos de sequência
Ops on Sequências
'''
# Definição de uma Tupla
tu = (23,'abc',4.56, (2,3),'def')

# Definição de uma List
li = ["abc", 34, 4.34, 23]

# Definição de String
st = "Hello"
st = 'Hello'
st = "Hello's"
st = 'Hello "My friend" '
st = """ 
        a', b, "c"   
     """

# Exemplo de definição de sequências e acesso a índices
tu = (23,'abc',4.56, (2,3),'def') # Define uma Tupla
# Acessa o índice 1
print(tu[1])

li = ["abc", 34, 4.35, 23] # Define uma Lista
print(li[1])

st = "Hello World"
print(st[1])

# Exemplos de acesso a índices negativos
t = (23, 'abc', 4.56, (2,3), 'def') # Define uma Tupla
print(t[1])

# Acessando índice negativo -3
print(t[-3]) 

# Exemplo de extração de subconjunto de uma sequência
t =  (23, 'abc', 4.56, (2,3), 'def')
# Acessando um subconjunto
print(t[1:4])

# Também podemos usar índices negativos
print(t[1:-1])

# Exemplo de extração de subconjunto de uma sequência
t =  (23, 'abc', 4.56, (2,3), 'def')
# Também podemos omitir um índice
print(t[:2])
print(t[2:])

# Exemplo de extração de subconjunto de uma sequência
# Cópia das entradas de um sequência
list1 = [23, 'abc', 4.56, (2,3), 'def']
# Isso faz uma cópia da entradas de um sequência
list2 = list1    # 2 nome 1 referência
list1.append(100)
print(list2)
# Faz uma cópia da list1
list2 = list1[:] # Duas cópias independentes
list1.append('cópia')
print(list2)
print(list1)