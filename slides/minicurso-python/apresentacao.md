Introdução ao Python
===
- Instalação
- Primeiros Passos
- Básico
- Operadores e Expressões
- Controle de Fluxo
- Funções

---

Introdução ao Python
===
- Módulos
- Estruturas de Dados
- Orientação a Objetos
- Input e Output
- Exceções
- Bibliotecas Padrões

---

Instalação
===

---

Windows
===
[Página dos Insladores for Windows](https://www.python.org/downloads/windows/)
Baixar o instalador, next, next finish

---

Mac
===
[Página dos Insladores for MacOS](https://www.python.org/downloads/mac-osx/)
Sou pobre!!!
Mais acho que ele já instalado.

---

Linux
===
Já vem instalado na maioria das distro.
`> python`

---

Primeiros Passos
===

---

Interpretador Interativo
===
1. Abra o Terminal;
2. Iniciar o interpretador interativo python, no linux:
   `> python`
3. No prompt do interpretador `>>>` digite:
```
>>> print("Hello World!!")
    [enter]
>>> Hello World!!
```
4. CTRL-D para sair do interpretador.

---

Script
===
1. Criar arquivo helloworld.py
2. Abrir o arquivo no editor favorito
3. Inserir o seguinte código:
```
    #!/usr/bin/env python
    print("Hello World!!")
```

---

Tipos de Dados Básicos
===
```
    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    #Isso é um comentário em linha

    x=100     #Isso é um inteiro
    y="Hello" #Isso é uma string
    z=3.45    #Isso é um float

    '''
    Isso é um comentário
    em bloco e também é usado
    pelo docstring.
    '''
```

---

Tipos de Dados Básicos
===
```
>>> x = 100     #Isso é um inteiro
>>> x = 5/2     #Isso também será um inteiro
>>> y = "Hello" #Isso é uma string
>>> y = 'Hello' #Isso também
>>> y = "Hello's" #Isso também
>>> y = """a'b"c""" #Isso também
>>> z = 3.45    #Isso é um float
>>> if z == 3.45 or y == "Hello":
>>>     x = x + 1       # Operação de adição
>>>     y = y + "World" #Concatenação de String
>>> print x
>>> print y
```

---

¬¬ Código
===
- ` = ` Operador de Atribuição 
- ` == ` Operador de Comparação
- ` + - * / %` Operadores Aritméticos
> ` + ` Também usado para concatenação
> ` % ` Usado também na formatação de Strings
---

¬¬ Código
===
- ` and, or, not` Operadores Lógicos
- ` print ` Comando básico de impressão
> Tipo das variaveis não precisa ser declarado
> O Python descobre o tipo por conta própria.

---

Atribuição '='
===
+ Cria-se referências e não copias;
+ Nomes não possue tipos, objetos que tem tipos;
+ Python determina o tipo de acordo com a referência;
+ `x = 3` Assim se define uma variável;
+ Nomes sem referência, são coletadas;
+ `x, y = 2, 3` assim podemos fazer uma atribuição múltipla.

---

Regras P/ Nomes
===
+ Nomes são case sensitive e devem obdecer a esse padrão:
> `bob  Bob _bob  _2_bob_  bob_2 BoB`
+ Palavras reservadas
> ```
> and, assert, break, class, continue, def, del, elif,
> else, except, exec, finally, for, from, global, if,
> import, in, is, lambda, not, or, pass, print, raise,
> return, try, while
> ```

---

Espaço em Branco
===
+ Usado como Identação
+ Para posicionar novas linhas
+ `\` usado para quebra de linha
+ Não usamos `{}` para blocos
+ As linhas que tiverem menas identação estão fora de blocks
+ As linhas mais identação estão dentro de blocos

---

Referência
===
+ `x = y` isso não é uma cópia, mais sim uma referência
```
>>> a = [1,2,3] # a referenciando a lista [1,2,3]
>>> b = a       # b passa recebe o valor referenciado por a
>>> a.append(4) # isso muda a lista referenciado por a
>>> print(b)    # Surprise!!
```

---

Referência
===
+ `x = 3`
+ Primeiro é criado um inteiro e armazenado na memoria;
+ Depois o Nome `x` é criado
+ A referência na memoria armazena o valor 3
![Referência I](img/referencia.png)

---

Referência
===
+ O dado 3 que criamos é do tipo integer. No python tipos de dados como integre, float, string e tuplas são "imutavéis";
+ Isso não que dirzer não podemos alterar o valor que `x` se refere;
+ Ex:
```
>>> x = 3;
>>> x = x + 1
>>> print(x)
```

---

Referência
===
Analisando o código
1. A referência ao nome `x` é procurada;
2. O valor dessa referência é recuperado;
3. O valor 3+1 ocorre, produzindo um novo elemento de dado 4, que é atribuído a um novo local de memória.
![Referência II](img/referenciaII.png)

---

Referência
===
```
>>> x = 3
>>> y = x
>>> y = 4
>>> print(x) # O que vai sair aqui?
```

---

Referência
===
![Referência IIIa](img/referenciaIIIa.png)
![Referência IIIb](img/referenciaIIIb.png)

---

Referência
===
![Referência IVa](img/referenciaIVa.png)
![Referência IVb](img/referenciaIVb.png)

---

Referência
===
![Referência Va](img/referenciaVa.png)
![Referência Vb](img/referenciaVb.png)

---

Referência
===
![Referência VIa](img/referenciaVIa.png)
![Referência VIb](img/referenciaVIb.png)

---

Referência
===
![Referência VIIa](img/referenciaVIIa.png)
![Referência VIIb](img/referenciaVIIb.png)

---

Referência
===
![Referência VIII](img/referenciaVIII.png)

---

Tipos Sequência
===
1. Tuple
   > Uma simples sequência imutável de itens mixáveis;
2. String - imutável
3. List
   > Uma sequência mutável de itens mixáveis;

---

Ex de Sequências
===
+ Tuplas são definidas entre parentese (val,val,..)
`tu = (23,'abc',4.56, (2,3),'def')`
+ List são definidas usando colchetes `[val,val,val]`
`li = ["abc", 34, 4.34, 23]`
+ Strings a gente já viu!!!

---

Ops on Sequências
===
```
# Exemplo de definição de sequências e acesso a índices
>>> tu = (23,'abc',4.56, (2,3),'def') # Define uma Tupla
>>> tu[1] # Acessa o índice 1
>>> 'abc' # Valor do índice 1

>>> li = ["abc", 34, 4.35, 23] # Define uma Lista
>>> li[1]
>>> '34'

>>> st = "Hello World"
>>> st[1]
>>> 'e'
```

---

Ops on Sequências
===
```
# Exemplos de acesso a índices negativos
>>> t = (23, 'abc', 4.56, (2,3), 'def') # Define uma Tupla
>>> t[1]
>>> 'abc'
>>> t[-3] # Acessando índice negativo -3
>>> 4.56
```

---

Ops on Sequências
===
```
# Exemplo de extração de subconjunto de uma sequência
>>> t =  (23, 'abc', 4.56, (2,3), 'def')
# Acessando um subconjunto
>>> t[1:4]
>>> ('abc', 4.56, (2,3))
# Também podemos usar índices negativos
>>> t[1:-1]
>>> ('abc', 4.56, (2,3))
```

---

Ops on Sequências
===
```
# Exemplo de extração de subconjunto de uma sequência
>>> t =  (23, 'abc', 4.56, (2,3), 'def')
# Também podemos omitir um índice
>>> t[:2]
>>> (23,'abc')
>>> t[2:]
>>> (4.56, (2,3),'def')
```

---

Ops on Sequências
===
```
# Exemplo de extração de subconjunto de uma sequência
# Cópia das entradas de um sequência
>>> t[:]
# Isso faz uma cópia da entradas de um sequência
>>> list2 = list1    # 2 nome 1 referência
>>> list2 = list1[:] # Duas cópias independentes
```

---

Operador 'in'
===
+ O operador 'in' é usado para testes boleanos;
+ Verificar se um elemento "faz parte de";
```
>>> t = [1,2,4,5]
>>> 3 in t
>>> false
>>> 4 in t
>>> True
>>> 4 not in t
>>> False
```

---

Operador 'in'
===
```
>>> a = 'abcde'
>>> 'c' in a
>>> True
>>> 'cd' in a
>>> True
>>> 'ac' in a
>>> False
```

---

Operador '+'
===
```
>>> (1, 2, 3) + (4, 5, 6)
>>> (1, 2, 3, 4, 5, 6)
>>> [1, 2, 3] + [4, 5, 6]
>>> [1, 2, 3, 4, 5, 6]
>>> “Hello” + “ ” + “World”
>>> 'H'llo World’
```

---

Mutabilidade
===
Tuples vs Lists
---

---

Ops on Lists
===
```
>>> li = [1,11,3,4,5]
>>> li.append('a')
>>> li
>>> [1, 11, 3, 4, 5, 'a']
>>> li.insert(2,'i')
>>> li
>>> [1, 11, 'i', 3, 4, 5, 'a']
>>> li.extend([9,8,7]) # E o operador + ?
>>> li.append([10, 11, 12]) # ?
```

---

Ops on Lists
===
```
>>> li = ['a', 'b', 'c', 'b']
>>> li.index('b') # O índice da primeira ocorrência
>>> 1
>>> li.count('b') # Número de ocorrências
>>> 2
>>> li.remove('b') # Remove a primeira ocorrência
>>> li
>>> ['a', 'c', 'b']

```

---

Ops on Lists
===
```
>>> li = [5, 2, 6, 8]
>>> li.reverse() # Inverte a posição
>>> li
    [8, 6, 2, 5]
>>> li.sort() # Ordena
>>> li
    [2, 5, 6, 8]
>>> li.sort() # Ordena com base em um função

```

---

Tuples vs Lists
===
```
>>> li = list(tu) # Converte um Tuple -> List
>>> tu = tuple(li) # Converte um List -> Tuple
```

---

Dicionários
===
+ Armazena valores que mapeão uma chave e um valor;
+ Chaves são imutáveis;
+ Valores podem ser de qualquer tipo;
+ Um dicionário pode armazenar valores de diferentes tipos;
+ Podemos definir, modificar, visualizar e deletar um par chave-valor;

---

Ops on Dicionários
===
```
    # Define um Dicionário
>>> d = {'user':'bozo', 'pswd':1234}
>>> d = ['user'] # O valor de um chave
>>> 'bozo'
>>> d['pswd']
>>> 1234
>>> d['bozo'] # ?
```

---

Ops on Dicionários
===
```
>>> d = {'user':'bozo', 'pswd':1234}
>>> d = ['user'] = 'clown' # Altera o valor
>>> d
>>> {'user':'clown', 'pswd':1234}
>>> d['id'] = 25 # Adiciona elemento
>>> d
>>> {'user':'bozo', 'id':45 ,'pswd':1234}
```

---

Ops on Dicionários
===
```
>>> d = {'user':'bozo', 'id':45 ,'pswd':1234} 
>>> del d['user'] # Remove um
>>> d
>>> {'id':45 ,'pswd':1234}
>>> d.clear() # Remove todos
>>> d
>>> {}
```

---

Ops on Dicionários
===
```
>>> d = {'user':'bozo', 'id':45 ,'pswd':1234} 
>>> d.keys()
>>> ['user','p','i']
>>> d.values()
>>> ['bozo', 1234, 34]
>>> d.items()
>>> [('user':'bozo'), ('id':45) ,('pswd':1234)]
```

---

Funções
===
+ `def` cria uma função e associa ao um nome
+ `return` envia o resultado de retorno;
+ Argumentos são passados por atribuição;
+ Tipos dos argumentos e do retorno não são declarados;
```
    # Estrutura de uma função
    def <name>(arg1, arg2, ..., argN)
        <statements>
        return <value>
    # Exemplo de declaração de função
    def times(x,y):
        return x*y
```

---

Funções
===
+ Argumentos são passados por atribuição
+ Argumentos passados são atribuídos a nomes locais
+ Atribuição a nomes de argumentos não afeta o chamador
+ Alterar um argumento mutável pode afetar o chamador

```
    def changer():
        x = 2       # Muda apenas o valor local x.
        y[0] 'hi'   # Muda o objeto compartilhado.
```

---

Controle de Fluxo
===
```
    # Exemplo de Controle de fluxo com IF
>>> if x == 3:
        print("x igual a 3.)
    elif x == 2:
        print("X igual 2.")
    else:
        print ("X igual outra coisa.)
    print("Isso esta fora do 'if'.")
```

---

Controle de Fluxo
===
```
    # Exemplo de Controle de fluxo com while
    x = 3
    while x < 10:
        if x > 7:
            x += 2
            continue
        x = x+1
        print("Ainda no loop.")
        if x == 8:
            break
    print("Isso esta fora do loop.)
>>> 
```

---

Controle de Fluxo
===
```
    # Exemplo de Controle de fluxo com for
>>> for x in range(10):
        if x > 7:
            x +=2
            continue
        x = x + 1
        print("Ainda no loop.")
        if x == 8:
            break
    print("Isso esta fora do loop.")
```

---

Módulos
===
+ Favorece o reuso de código
+ Rotinas podem ser chamadas várias vezes dentro de um programa;
+ Rotinas podem ser usadas por vários programas;
+ Organização em espaço de nomes;
+ Agrupa dados juntamente com funções usadas para esses dados;

---

Módulos
===
+ Módulos são funções e variaveis definidas em arquivos específicos;
+ Itens são importados usando `from` ou `import`
```
    from module import function
    function()

    import module
    module.function()
```
+ Modulos são considerados espaços de nomes;

---

Guias e Referências
===
+ [http://tdc-www.harvard.edu/Python.pdf](http://tdc-www.harvard.edu/Python.pdf)
+ [https://www.javatpoint.com/python-tutorial](https://www.javatpoint.com/python-tutorial)
+ [https://hackr.io/tutorials/learn-python](https://hackr.io/tutorials/learn-python)
+ [https://python.swaroopch.com/](https://python.swaroopch.com/)
+ [http://bokeh.pydata.org/en/latest/docs/gallery.html](http://bokeh.pydata.org/en/latest/docs/gallery.html)