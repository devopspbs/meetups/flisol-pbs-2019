# Slides do Flisol Parauapebas 2019

## Palestras

Abertura - Hélio Bentzen

[Conceitos Fundamentais de Software Livre - Tiago Rocha](https://tiagorocha.gitlab.io/talk-software-livre/)

[Por que Containers - Thiago Almeida](https://gitlab.com/devopspbs/flisol-pbs-2019/tree/master/slides/Apresentacao-Containers.odp)

[Microsoft OpenSource! Como isso vai afetar sua vida? - William Breno Begot Moura](https://gitlab.com/devopspbs/flisol-pbs-2019/raw/master/slides/Microsoft%20OpenSource%20-%20William%20Begot%20-%20FLISoL%202019.pdf)

Analisando dados com Jupyter Notebook no Databricks Community - Dhony Silva

[Geocity: Ferramenta on-line de disponibilização de dados municipais coletados em fontes públicas (dados abertos) - Jorge Clesio](https://gitlab.com/devopspbs/flisol-pbs-2019/raw/master/slides/GEOCITY.pdf)

## Minicursos

[Distros GNU/Linux para Desktop Autor - Tiago Rocha](https://gitlab.com/devopspbs/flisol-pbs-2019/tree/master/slides/minicurso-gnu-linux/)

[Introdução ao Python - Thiago Almeida](https://gitlab.com/devopspbs/flisol-pbs-2019/tree/master/slides/minicurso-python/) 
